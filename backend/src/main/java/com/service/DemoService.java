package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Demo;
import com.model.DemoInfo;
import com.repository.DemoRepository;
@Service
public class DemoService {
	@Autowired
	private DemoRepository demoRepository;
	
	public Demo findById(Long id) {
        return demoRepository.findById(id);
    }
 
    public List<DemoInfo> listDemoInfo() {
        return demoRepository.listDemoInfo();
    }
}
