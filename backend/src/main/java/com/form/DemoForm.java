package com.form;

public class DemoForm {
	private Long id;
	private String name;
	private double category;

	public DemoForm(Long id, String name, double category) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
	}
}
