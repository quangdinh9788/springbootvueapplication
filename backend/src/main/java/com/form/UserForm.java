package com.form;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserForm {
	private Long id;
	private String username;
	private String email;
	private String password;
	
	private Set<String> role;
}
