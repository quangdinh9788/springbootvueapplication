package com.form;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtForm {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private Set<String> roles;
	
	public JwtForm(String accessToken, Long id, String username, String email, Set<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}

}
