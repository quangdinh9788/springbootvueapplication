package com.model;

public enum ERole {
    ROLE_DEVELOPER, ROLE_ADMIN
}