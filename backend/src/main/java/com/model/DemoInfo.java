package com.model;

import lombok.Data;

@Data
public class DemoInfo {
	private Long id;
	private String name;
	private String category;

	public DemoInfo(Long id, String name, String category) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
	}

}
