package com.constants;

public class Constants {
	public static class Role{
		public static final String ADMIN = "admin";
		public static final String DEVELOPER = "developer";
	}
}
