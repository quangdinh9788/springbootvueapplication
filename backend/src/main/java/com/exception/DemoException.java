package com.exception;

public class DemoException extends Exception {

	private static final long serialVersionUID = -3128681006635769411L;

	public DemoException(String message) {
		super(message);
	}
}