package com.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.form.UserFormResponse;
import com.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/all")
	public String allAccess() {
		return "Public Content.";
	}

	@GetMapping("/dev")
	@PreAuthorize("hasRole('DEVELOPER')")
	public String developerAccess() {
		log.info("developerAccess");
		return "Developer Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		log.info("adminAccess");
		return "Admin Board.";
	}

	@GetMapping("/user/all")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAccountList() {
		long begin = System.currentTimeMillis();
		List<User> lstUser = userRepository.findAll();
		long begin1 = System.currentTimeMillis();
		List<UserFormResponse> result = lstUser.stream().map(this::convertToDto).collect(Collectors.toList());
		long begin2 = System.currentTimeMillis();
		System.out.println("action1:"+(begin1 - begin)+"/action2:"+(begin2 - begin1));
		return ResponseEntity.ok(result);
	}

	private UserFormResponse convertToDto(User user) {
		UserFormResponse postDto = modelMapper.map(user, UserFormResponse.class);
		return postDto;
	}
	
	@GetMapping("/user/{username}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAccountInfo(@PathVariable String username) {
		Optional<User> user = userRepository.findByUsername(username);
		UserFormResponse res = convertToDto(user.orElse(null));
		return ResponseEntity.ok(res);
	}

}
