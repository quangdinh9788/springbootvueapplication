package com.repository;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entity.Demo;
import com.model.DemoInfo;

@Repository
@Transactional
public class DemoRepository {
	@Autowired
    private SessionFactory sessionFactory;
	
	public DemoRepository() {
		
	}
	
	public Demo findById(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.get(Demo.class, id);
    }
 
    public List<DemoInfo> listDemoInfo() {
        String sql = "Select new " + DemoInfo.class.getName() //
                + "(e.id,e.name,e.category) " //
                + " from " + Demo.class.getName() + " e ";
        Session session = this.sessionFactory.getCurrentSession();
        Query<DemoInfo> query = session.createQuery(sql, DemoInfo.class);
        return query.getResultList();
    }
 
//    // MANDATORY: Giao dịch bắt buộc phải được tạo sẵn trước đó.
//    @Transactional(propagation = Propagation.MANDATORY)
//    public void addAmount(Long id, double amount) throws DemoException {
//        Demo account = this.findById(id);
//        if (account == null) {
//            throw new DemoException("Account not found " + id);
//        }
//        double newBalance = account.getBalance() + amount;
//        if (account.getBalance() + amount < 0) {
//            throw new DemoException(
//                    "The money in the account '" + id + "' is not enough (" + account.getBalance() + ")");
//        }
//        account.setBalance(newBalance);
//    }
// 
//    // Không được bắt BankTransactionException trong phương thức này.
//    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = DemoException.class)
//    public void sendMoney(Long fromAccountId, Long toAccountId, double amount) throws DemoException {
// 
//        addAmount(toAccountId, amount);
//        addAmount(fromAccountId, -amount);
//    }
}
