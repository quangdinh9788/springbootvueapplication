const path = require('path');

module.exports = {
  devtool: 'source-map',
  resolve: {
    alias: {
      Views: path.resolve(__dirname, 'src/views/'),
      Components: path.resolve(__dirname, 'src/components')
    }
  }
};