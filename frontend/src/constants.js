const SERVER_URL = "http://localhost:38881/";
const API_URL = SERVER_URL +"api/";
const AUTH_URL = API_URL +"auth/";
const USER_URL = API_URL + "user/";
const ROLE = {"ADMIN":"ROLE_ADMIN", "DEVELOPER": "ROLE_DEVELOPER"}
const UTILS_PATH = "@/utils/"

export default {
    SERVER_URL: SERVER_URL,
    API_URL: API_URL,
    AUTH_URL: AUTH_URL,
    ROLE: ROLE,
    USER_URL: USER_URL,
    UTILS_PATH : UTILS_PATH
}