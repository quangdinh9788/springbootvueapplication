import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
Vue.use(Router);
import { i18n, loadLanguageAsync} from '@/i18n/i18n-setup';


export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/profile',
      name: 'profile',
      // lazy-loaded
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      children: [
        {
          path: '/admin/accountList',
          name: 'admin-accountList',
          // lazy-loaded
          component: () => import('./views/admin/AccountList.vue')
        },
        {
          path: '/admin/accountEdit/',
          name: 'admin-accountL-edit',
          // lazy-loaded
          component: () => import('./views/admin/AccountEdit.vue')
        }
      ],
      // lazy-loaded
      component: () => import('./views/BoardAdmin.vue')
    },    
    {
      path: '/dev',
      name: 'developer',
      // lazy-loaded
      component: () => import('./views/BoardDeveloper.vue')
    },

    // demo
    {
      path: '/demo',
      name: 'demo',
      children: [
        {
          path: '/demo/slot',
          name: 'demo_slot',
          // lazy-loaded
          component: () => import('@/views/demo/slot/Parent.vue')
        },{
          path: '/demo/composition',
          name: 'demo_composition',
          // lazy-loaded
          component: () => import('@/views/demo/composition/Parent.vue')
        },
        {
          path: '/demo/composition1',
          name: 'demo_composition1',
          // lazy-loaded
          component: () => import('@/views/demo/composition1/App.vue')
        },
        {
          path: '/demo/vee-validate',
          name: 'demo_vee-validate',
          // lazy-loaded
          component: () => import('@/views/demo/vee-validate/VeeValidate.vue')
        },
        {
          path: '/demo/dynamic-component',
          name: 'demo_dynamic-component',
          // lazy-loaded
          component: () => import('@/views/demo/dynamic-component/DynamicComponent.vue')
        },
        {
          path: '/demo/dynamic-component1',
          name: 'demo_dynamic-component1',
          // lazy-loaded
          component: () => import('@/views/demo/dynamic-component1/DynamicComponent1.vue')
        },
        {
          path: '/demo/pug-including',
          name: 'demo_pug-including',
          // lazy-loaded
          component: () => import('@/views/demo/pug/Including.vue')
        },
        {
          path: '/demo/pug-extending',
          name: 'demo_pug-extending',
          // lazy-loaded
          component: () => import('@/views/demo/pug/Extending.vue')
        },
        {
          path: '/demo/pug-event',
          name: 'demo_pug-event',
          // lazy-loaded
          component: () => import('@/views/demo/pug/Event.vue')
        },
        {
          path: '/demo/pug-events',
          name: 'demo_pug-events',
          // lazy-loaded
          component: () => import('@/views/demo/pug/EventsTemplate.vue')
        },
        {
          path: '/demo/v-on',
          name: 'demo_v-on',
          // lazy-loaded
          component: () => import('@/views/demo/v-on/Von.vue')
        }
      ],
      // lazy-loaded
      component: () => import('@/views/demo/Demo.vue')
    },
     // basic component
    {
      path: '/basic-components',
      name: 'basic-components',
      children: [
        {
          path: '/basic-components/input-text',
          name: 'basic-components_input-text',
          // lazy-loaded
          component: () => import('@/views/basic-components/input-text/InputTextDemo.vue')
        },
        {
          path: '/basic-components/input-switch',
          name: 'basic-components_input-switch',
          // lazy-loaded
          component: () => import('@/views/basic-components/input-switch/InputSwitchDemo.vue')
        },
        {
          path: '/basic-components/input-number',
          name: 'basic-components_input-number',
          // lazy-loaded
          component: () => import('@/views/basic-components/input-number/InputNumberDemo.vue')
        },
        {
          path: '/basic-components/inherit-attrs',
          name: 'basic-components_inherit-attrs',
          // lazy-loaded
          component: () => import('@/views/basic-components/inherit-attrs/InheritAttrsDemo.vue')
        },
        {
          path: '/basic-components/custom-listeners',
          name: 'basic-components_custom-listeners',
          // lazy-loaded
          component: () => import('@/views/basic-components/custom-listeners/CustomListenerDemo.vue')
        },
        {
          path: '/basic-components/radio-button',
          name: 'basic-components_radio-button',
          // lazy-loaded
          component: () => import('@/views/basic-components/radio-button/RadioButtonDemo.vue')
        },
        {
          path: '/basic-components/multi-select',
          name: 'basic-components_multi-select',
          // lazy-loaded
          component: () => import('@/views/basic-components/multi-select/MultiSelectDemo.vue')
        },
        {
          path: '/basic-components/slot-name',
          name: 'basic-components_slot-name',
          // lazy-loaded
          component: () => import('@/views/basic-components/slot-name/SlotNameDemo.vue')
        }
      ],
      // lazy-loaded
      component: () => import('@/views/basic-components/Components.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register', '/home', '/demo'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = sessionStorage.getItem('user');
  // const lang = to.params.lang;
  const lang = i18n.locale;
  console.log('loggedIn:', sessionStorage.getItem('user'), '/lang:', lang)
  loadLanguageAsync(lang).then(() => next());
  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
});