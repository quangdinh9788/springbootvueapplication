import Vue from 'vue';
import App from './App.vue';
import { router } from './router';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// import VeeValidate from 'vee-validate';
import { dom, library } from '@fortawesome/fontawesome-svg-core';
dom.watch()
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { ClientTable } from 'vue-tables-2';
import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faAddressCard,
  faGlobe,
  faCube,
  faAngleDown,
  faTimes,
  faCheck
} from '@fortawesome/free-solid-svg-icons';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt, faAddressCard, faGlobe, faCube, faAngleDown, faTimes, faCheck);
library.add(fas);
Vue.config.productionTip = false;
// Vue.use(VeeValidate);
// Vue.use(VeeValidate, {
//   errorBagName: 'vErrors'
// });
Vue.use(ClientTable);
Vue.component('font-awesome-icon', FontAwesomeIcon);
import VueCompositionApi from '@vue/composition-api';
Vue.use(VueCompositionApi);

import {i18n} from '@/i18n/i18n-setup.js';
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app');