import Item from './item';
export default class SubMenuItem extends Item {
  constructor(label, template, path, icon, func, items) {
    super(label, template, path, icon, func);
    this.items = items;
  }
}