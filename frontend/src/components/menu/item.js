export default class Item {
    constructor(label, template, path, icon,func, params) {
      this.label = label;
      this.template = template;
      this.path = path;
      this.icon = icon;
      this.func = func;
      this.params = params
    }
    doAction(){
      this.func(this.params);
    }
  }