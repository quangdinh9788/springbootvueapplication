import { reactive } from '@vue/composition-api'
export default function () {

    const state = reactive({
        zindex: 999
    });

    function hasClass(element, className) {
        if (element.classList) return element.classList.contains(className); else return new RegExp('(^| )' + className + '( |$)', 'gi').test(element.className);
    }

    function generateZIndex() {
        return ++state.zindex;
    }

    function getHiddenElementDimensions(element) {
        var dimensions = {};
        element.style.visibility = 'hidden';
        element.style.display = 'block';
        dimensions.width = element.offsetWidth;
        dimensions.height = element.offsetHeight;
        element.style.display = 'none';
        element.style.visibility = 'visible';
        return dimensions;
    }

    function getWindowScrollTop() {
        var doc = document.documentElement;
        return (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    }

    function getWindowScrollLeft() {
        var doc = document.documentElement;
        return (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    }

    function getViewport() {
        var win = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            w = win.innerWidth || e.clientWidth || g.clientWidth,
            h = win.innerHeight || e.clientHeight || g.clientHeight;
        return {
            width: w,
            height: h
        };
    }

    function relativePosition(element, target) {
        var elementDimensions = element.offsetParent ? {
            width: element.offsetWidth,
            height: element.offsetHeight
        } : getHiddenElementDimensions(element);
        var targetHeight = target.offsetHeight;
        var targetWidth = target.offsetWidth;
        var targetOffset = target.getBoundingClientRect();
        var viewport = getViewport();
        var top, left;

        if (targetOffset.top + targetHeight + elementDimensions.height > viewport.height) {
            top = -1 * elementDimensions.height;
            element.style.transformOrigin = 'bottom';
        } else {
            top = targetHeight;
            element.style.transformOrigin = 'top';
        }

        if (targetOffset.left + elementDimensions.width > viewport.width) left = targetWidth - elementDimensions.width; else left = 0;
        element.style.top = top + 'px';
        element.style.left = left + 'px';
    }

    function getOuterWidth(el, margin) {
        if (el) {
            var width = el.offsetWidth;

            if (margin) {
                var style = getComputedStyle(el);
                width += parseFloat(style.marginLeft) + parseFloat(style.marginRight);
            }

            return width;
        } else {
            return 0;
        }
    }

    function absolutePosition(element, target) {
        var elementDimensions = element.offsetParent ? {
            width: element.offsetWidth,
            height: element.offsetHeight
        } : getHiddenElementDimensions(element);
        var elementOuterHeight = elementDimensions.height;
        var elementOuterWidth = elementDimensions.width;
        var targetOuterHeight = target.offsetHeight;
        var targetOuterWidth = target.offsetWidth;
        var targetOffset = target.getBoundingClientRect();
        var windowScrollTop = getWindowScrollTop();
        var windowScrollLeft = getWindowScrollLeft();
        var viewport = getViewport();
        var top, left;

        if (targetOffset.top + targetOuterHeight + elementOuterHeight > viewport.height) {
            top = targetOffset.top + windowScrollTop - elementOuterHeight;
            element.style.transformOrigin = 'bottom';
        } else {
            top = targetOuterHeight + targetOffset.top + windowScrollTop;
            element.style.transformOrigin = 'top';
        }

        if (targetOffset.left + targetOuterWidth + elementOuterWidth > viewport.width) left = targetOffset.left + windowScrollLeft + targetOuterWidth - elementOuterWidth; else left = targetOffset.left + windowScrollLeft;
        element.style.top = top + 'px';
        element.style.left = left + 'px';
    }

    return {
        hasClass,
        generateZIndex,
        absolutePosition,
        relativePosition,
        getOuterWidth
    }
}