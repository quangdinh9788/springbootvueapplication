export default function () {
    function parseValue(text) {
        let filteredText = text
            .trim()
            .replace(/\s/g, "")
            .replace(this._currency, "")
            .replace(this._group, "")
            .replace(this._suffix, "")
            .replace(this._prefix, "")
            .replace(this._minusSign, "-")
            .replace(this._decimal, ".")
            .replace(this._numeral, this._index);

        if (filteredText) {
            let parsedValue = +filteredText;
            return isNaN(parsedValue) ? null : parsedValue;
        }

        return null;
    }

    function validateValue(value) {
        if (this.min != null && value < this.min) {
            return this.min;
        }

        if (this.max != null && value > this.max) {
            return this.max;
        }

        return value;
    }

    return {
        parseValue,
        validateValue        
    }
}