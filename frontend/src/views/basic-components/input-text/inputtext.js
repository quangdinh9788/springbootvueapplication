import { ref, watch } from '@vue/composition-api';
export default function (props, {emit}){
    const xinput1 = ref(props.value);
    function doClick(event){
        if(props.listeners['click']){
            props.listeners['click'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }

    if(props.twoWay){
        watch(xinput1, value1 => {
            emit("input", value1);
        });
    }

    function doOnChange(event){
        if(props.listeners['change']){
            props.listeners['change'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }
    return {
        xinput1,
        doClick,
        doOnChange
    }
}