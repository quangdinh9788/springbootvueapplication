import { reactive } from '@vue/composition-api'
export default function(listeners){
    if(!listeners){
        listeners = reactive({
            'change': [],
            'focus': [],
            'blur': [],
            'keydown': [],
            'keyup': []
        });
    }
    listeners['change'].push({'name': 'doChange'});
    listeners['focus'].push({'name': 'doFocus'});
    listeners['blur'].push({'name': 'doBlur'});
    listeners['keydown'].push({'name': 'doKeydown'});
    listeners['keyup'].push({'name': 'doKeyup'});
    return {
        listeners
    }
}