import { ref, watch } from '@vue/composition-api';
export default function (props, {emit}){
    const input = ref(props.value);

    function onChange(event){
        if(props.listeners['change']){
            props.listeners['change'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }

    function onBlur(event){
        if(props.listeners['blur']){
            props.listeners['blur'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }

    function onFocus(event){
        if(props.listeners['focus']){
            props.listeners['focus'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }

    function onKeyup(event){
        if(props.listeners['keyup']){
            props.listeners['keyup'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }

    function onKeydown(event){
        if(props.listeners['keydown']){
            props.listeners['keydown'].forEach((listener) => {
                emit(listener.name, {'name': props.name, 'value':event.target.value, 'event': event})
            })
        }
    }
    if(props.twoWay){
        watch(input, value => {
            emit("input", value);
        });
    }

    
    return {
        input,
        onChange,
        onBlur,
        onFocus,
        onKeyup,
        onKeydown
    }
}