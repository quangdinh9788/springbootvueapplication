import axios from 'axios';
import Constants from '../../constants';
import authHeader from '../jwt-authen/auth-header';

class AccountService {
  getAccountList(){
    return axios.get(Constants.USER_URL + 'all', { headers: authHeader() });
  }
  getAccountEdit(username){
    console.log('username:', username)
    return axios.get(Constants.USER_URL + username, { headers: authHeader()});
  }
}

export default new AccountService();