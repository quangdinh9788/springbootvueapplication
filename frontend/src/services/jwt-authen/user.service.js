import axios from 'axios';
import Constants from '../../constants';
import authHeader from './auth-header';

class UserService {
  getPublicContent() {
    return axios.get(Constants.API_URL + 'all');
  }

  getDeveloperBoard() {
    return axios.get(Constants.API_URL + 'dev', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(Constants.API_URL + 'admin', { headers: authHeader() });
  }
}

export default new UserService();