import axios from "axios";

const DEMO_API_URL = "http://localhost:38881";
const DEMO_URL = `${DEMO_API_URL}/demo/list`;

class CourseDataService {
  retrieveAllCourses() {
    return axios.get(`${DEMO_URL}`);
  }
}

export default new CourseDataService();