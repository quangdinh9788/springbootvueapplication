import Constants from '@/constants.js'
import { reactive, computed } from '@vue/composition-api'

export default function ($this){
    console.log('$this:', $this)
    console.log('this:', this)
    const state = reactive({
        currentUser: computed(() => $this.$store.state.auth.user),
        hasRoleAdmin: computed(() => $this.currentUser && $this.currentUser.roles && $this.currentUser.roles.includes(Constants.ROLE.ADMIN))
    });
    return {
        currentUser: state.currentUser,
        hasRoleAdmin: state.hasRoleAdmin,
    }
}
// import { reactive, computed } from '@vue/composition-api'

// export default function() {
//   const state = reactive({
//     count: 0,
//     double: computed(() => state.count * 2)
//   });

//   function increment() {
//     state.count++
//   }

//   return {
//     count: state.count,
//     double: state.double,
//     increment
//   }
// }