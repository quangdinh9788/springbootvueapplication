import Vue from 'vue'
import VueI18n from 'vue-i18n'
import enMessages from '@/i18n/messages/en.json'
import axios from 'axios'

Vue.use(VueI18n)
const messages ={
    en: enMessages
}

const defaultLanguage = 'en';
export const i18n = new VueI18n({
  locale: defaultLanguage, // set locale
  messages,
  fallbackLocale: defaultLanguage,
})

const supportedLanguages = [defaultLanguage, 'vn'];
const loadedLanguages = [defaultLanguage] // our default language that is preloaded

function setI18nLanguage (lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  console.log('setI18nLanguage:', lang);
  return lang
}

export function loadLanguageAsync(lang) {
  if(!supportedLanguages.includes(lang)){
    lang = defaultLanguage;
  }
  // If the same language
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang))
  }
  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }
  // If the language hasn't been loaded yet
  return import(/* webpackChunkName: "lang-[request]" */ `@/i18n/messages/${lang}.json`).then(
    messages => {
      console.log('set language:', lang)
      i18n.setLocaleMessage(lang, messages.default)
      loadedLanguages.push(lang)
      return setI18nLanguage(lang)
    }
  )
}